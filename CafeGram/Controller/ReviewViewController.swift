//
//  ReviewViewController.swift
//  CafeGram
//
//  Created by Md. Kamrul Hasan on 12/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    @IBOutlet var rateButtons: [UIButton]!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var cafe: CafeMO!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let cafeImage = cafe.image {
            backgroundImageView.image = UIImage(data: cafeImage as Data)
        }
        
        // appplying the blur effect
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
        
        let moveRightTransform = CGAffineTransform.init(translationX: 600, y: 0)
        // make the buttons invisibles
        for rateBatton in rateButtons {
            rateBatton.transform = moveRightTransform
            rateBatton.alpha = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [], animations:{
            self.rateButtons[0].alpha = 1.0
            self.rateButtons[0].transform = .identity
        },completion: nil )
        
        UIView.animate(withDuration: 0.4, delay: 0.15, options: [], animations:{
            self.rateButtons[1].alpha = 1.0
            self.rateButtons[1].transform = .identity
        },completion: nil )
        
        UIView.animate(withDuration: 0.4, delay: 0.20, options: [], animations:{
            self.rateButtons[2].alpha = 1.0
            self.rateButtons[2].transform = .identity
        },completion: nil )
        
        UIView.animate(withDuration: 0.4, delay: 0.25, options: [], animations:{
            self.rateButtons[3].alpha = 1.0
            self.rateButtons[3].transform = .identity
        },completion: nil )
        
        UIView.animate(withDuration: 0.4, delay: 0.30, options: [], animations:{
            self.rateButtons[4].alpha = 1.0
            self.rateButtons[4].transform = .identity
        },completion: nil )
        
        UIView.animate(withDuration: 0.4, delay: 0.35, options: [], animations:{
            self.rateButtons[5].alpha = 1.0
            self.rateButtons[5].transform = .identity
        },completion: nil )
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
