//
//  CafeDetailViewController.swift
//  CafeGram
//
//  Created by Md. Kamrul Hasan on 12/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import CoreData

class CafeDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: CafeDetailHeaderView!
    
    var cafe: CafeMO!

    override func viewDidLoad() {
        super.viewDidLoad()
        // configure the tableview
        tableView.dataSource = self
        tableView.delegate = self

        // Do any additional setup after loading the view.
        headerView.nameLabel.text = cafe.name
        headerView.typeLabel.text = cafe.type
        
        if let cafeImage = cafe.image {
            headerView.headerImageView.image = UIImage(data: cafeImage as Data)
        }
        
        // customize the navigation bar
        navigationItem.largeTitleDisplayMode = .never
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .white
        navigationController?.hidesBarsOnSwipe = false
        
        tableView.contentInsetAdjustmentBehavior = .never
        
        // rating bar
        if let rating = cafe.rating {
            headerView.ratingImageView.image = UIImage(named: rating)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CafeDetailIconTextCell.self), for: indexPath) as! CafeDetailIconTextCell
            cell.iconImageView.image = UIImage(named: "phone")
            cell.shortTextLabel.text = cafe.phone
            return cell
        
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CafeDetailIconTextCell.self), for: indexPath) as! CafeDetailIconTextCell
            cell.iconImageView.image = UIImage(named: "map")
            cell.shortTextLabel.text = cafe.location
            return cell
        
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CafeDetailTextCell.self), for: indexPath) as! CafeDetailTextCell
                cell.descriptionLabel.text = cafe.summary
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CafeDetailSeparatorCell.self), for: indexPath) as! CafeDetailSeparatorCell
            cell.titleLabel.text = "How To Get Here"
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CafeDetailMapCell.self), for: indexPath) as! CafeDetailMapCell
            if let cafeLocation = cafe.location {
            cell.configure(location: cafeLocation)
            }
            return cell
            
        default:
            fatalError("Failor to instantiate the table view cell for detail view controller.")
        }
    }
    
    // navigation theme color
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let destinationController = segue.destination as! MapViewController
            destinationController.cafe = cafe
        }
        
        if segue.identifier == "showReview" {
            let destinationController = segue.destination as! ReviewViewController
            destinationController.cafe = cafe
        }
    }
    
    // MARK: close button for rating bar
    @IBAction func close(segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rateCafe(segue: UIStoryboardSegue){
        dismiss(animated: true) {
            if let rating = segue.identifier {
                self.cafe.rating = rating
                self.headerView.ratingImageView.image = UIImage(named: rating)
            }
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                appDelegate.saveContext()
            }
            self.headerView.ratingImageView.transform = .identity
            self.headerView.ratingImageView.alpha = 1
        }
    }
    
}
