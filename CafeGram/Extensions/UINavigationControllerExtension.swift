//
//  UINavigationControllerExtension.swift
//  CafeGram
//
//  Created by Md. Kamrul Hasan on 13/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var childForStatusBarStyle: UIViewController? {
        return topViewController
    }
}
