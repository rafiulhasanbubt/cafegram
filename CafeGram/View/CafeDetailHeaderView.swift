//
//  CafeDetailHeaderView.swift
//  CafeGram
//
//  Created by Md. Kamrul Hasan on 13/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class CafeDetailHeaderView: UIView {
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
